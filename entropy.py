#!/usr/bin/python3

from math import log2
import os
import sys
from time import sleep

getch_available = True
try:
    import getch
except ModuleNotFoundError:
    print("It seems like you don't have the getch library installed!")
    print("This program can function without getch, but if you don't want")
    print("to press ENTER after every letter you guess, install it with")
    print()
    print("  pip install getch")
    print()
    print("(or press ENTER to continue without getch)")
    input()
    getch_available = False

def my_input(s):
    if getch_available:
        print(s, end = '')
        sys.stdout.flush()
        r = getch.getche()
        print()
        return r
    else:
        print(s, end = '')
        sys.stdout.flush()
        return input()


text = "Con la diffusione dei cellulari su larga scala e del conseguente fenomeno all'interno di ogni aspetto quotidiano della vita dei giapponesi, sono state stabilite delle precise norme comportamentali da seguire e rispettare per non arrecare fastidio o disagio alle persone intorno"
# text = "test"

valid_chars = "abcdefghijklmnopqrstuvwxyz "

text = list(filter(lambda c: c in valid_chars, text.lower()))

guess_dist = {}
for i in range(len(text)):
    remaining_chars = set(valid_chars)
    cnt = 0
    while len(remaining_chars) > 1:
        os.system('cls' if os.name == 'nt' else 'clear')
        print("Guessed text:")
        print("".join(text[:i]).replace(" ", "_"))
        print("\nRemaining characters to guess:")
        print("".join(sorted(remaining_chars)).replace(" ", "_"))
        guess = set(my_input("\nYour guess: ")[0])
        cnt += 1
        if text[i] in guess:
            remaining_chars &= guess
            print("guessed right")
        else:
            remaining_chars -= guess
            print("guessed wrong")
        sleep(0.3)
    guess_dist[cnt] = guess_dist.get(cnt, 0) + 1

tot = sum(guess_dist[i] for i in guess_dist)
p = lambda i: guess_dist[i] / tot
entropy = - sum(p(i) * log2(p(i)) for i in guess_dist)

print(f"\nentropy per letter: {entropy} bits")
